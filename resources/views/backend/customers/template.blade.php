<x-backend.layouts.master>

<div class="row clearfix dashboard-items">
    <div class="col-md-4">
        <div class="card p-2 shadow-sm  rounded-3">
            <div class="body text-center">
                <a href="{{ route('customers.index') }}" target="_blank">
                    <i class="fa-solid fa-address-book mt-3" style="font-size: 10em;"></i>
                    <h5 class="mt-2">Customers List</h5>
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card p-2 shadow-sm  rounded-3">
            <div class="body text-center">
                <a href="{{ route('customers.show') }}" target="_blank">
                    <i class="fa fa-book mt-3" style="font-size: 10em;"></i>
                    <h5 class="mt-2">Customers Details</h5>
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card p-2 shadow-sm  rounded-3">
            <div class="body text-center">
                <a href="{{ route('customers.create') }}" target="_blank">
                    <i class="fa-solid fa-file-pen mt-3" style="font-size: 10em;"></i>
                    <h5 class="mt-2">Create Customers</h5>
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card p-2 shadow-sm  rounded-3">
            <div class="body text-center">
                <a href="{{ route('customers.edit') }}" target="_blank">
                    <i class="fa fa-pencil mt-3" style="font-size: 10em;"></i>
                    <h5 class="mt-2">Edit Customers</h5>
                </a>
            </div>
        </div>
    </div>
    {{-- <div class="col-md-4">
        <div class="card p-2 shadow-sm  rounded-3">
            <div class="body text-center">
                <a href="{{ route('customers.edit') }}" target="_blank">
                    <i class="fa-regular fa-pen-to-square mt-3" style="font-size: 10em;"></i>
                    <h5 class="mt-2">Role Update</h5>
                </a>
            </div>
        </div>
    </div> --}}
    <div class="col-md-8">
        <div class="card p-2 shadow-sm  rounded-3">
            <div class="body text-center">
                <a href="{{ route('customers.destroy') }}" target="_blank">
                    <i class="fa-regular fa-trash-can mt-3" style="font-size: 10em;"></i>
                    <h5 class="mt-2">Delete Customers</h5>
                </a>
            </div>
        </div>
    </div>
    {{-- <div class="col-md-3">
        <div class="card">
            <div class="body text-center">
                <a href="/studentportal/studentsPayable/show" target="_blank">
                    <i class="fa fa-hand-o-left" style="font-size: 10em;"></i>
                    <h5>Payable List</h5>
                </a>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card">
            <div class="body text-center">
                <a href="/studentportal/studentsPayment/show" target="_blank">
                    <i class="fa fa-hand-o-right" style="font-size: 10em;"></i>
                    <h5>All Payments</h5>
                </a>
            </div>
        </div>
    </div> --}}
</div>
</x-backend.layouts.master>