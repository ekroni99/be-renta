<?php

use App\Http\Controllers\CustomerController;
use App\Http\Controllers\RoleController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/
Route::get('/dashboard',function(){
    return view('backend.hudai');
})->name('dashboard');
//for Roles
Route::group(['prefix'=>'roles','as'=>'roles.'], function(){
    Route::get('/', [RoleController::class, 'roleIndex'])->name('index');
    Route::get('/create', [RoleController::class, 'roleCreate'])->name('create');
    Route::post('/store', [RoleController::class, 'roleStore'])->name('store');
    Route::get('/show', [RoleController::class, 'roleShow'])->name('show');
    Route::get('/edit', [RoleController::class, 'roleEdit'])->name('edit');
    Route::patch('/update', [RoleController::class, 'roleUpdate'])->name('update');
    Route::patch('/delete', [RoleController::class, 'roleDestroy'])->name('destroy');
});

//for Customers
Route::group(['prefix'=>'customers','as'=>'customers.'], function(){
    Route::get('/', [CustomerController::class, 'customerIndex'])->name('index');
    Route::get('/create', [CustomerController::class, 'customerCreate'])->name('create');
    Route::post('/store', [CustomerController::class, 'customerStore'])->name('store');
    Route::get('/show', [CustomerController::class, 'customerShow'])->name('show');
    Route::get('/edit', [CustomerController::class, 'customerEdit'])->name('edit');
    Route::patch('/update', [CustomerController::class, 'customerUpdate'])->name('update');
    Route::patch('/delete', [CustomerController::class, 'customerDestroy'])->name('destroy');
});