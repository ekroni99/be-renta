<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function customerIndex(){
        return view('backend.customers.template');
    }
    public function customerShow(){
        return view('backend.customers.show');
    }

    public function customerCreate(){
        return view('backend.customers.create');
    }

    public function customerStore(){
        
    }

    public function customerEdit(){
        return view('backend.customers.edit');
    }

    public function customerUpdate(){

    }

    public function customerDestroy(){

    }
}
