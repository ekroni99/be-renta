<?php

namespace App\Http\Controllers;

use App\Models\Role;
use Illuminate\Http\Request;

class RoleController extends Controller
{
    public function roleIndex(){
        return view('backend.roles.template');
    }
    public function roleShow(){
        return view('backend.roles.show');
    }

    public function roleCreate(){
        return view('backend.roles.create');
    }

    public function roleStore(Request $request){
        $request->validate([
            'roles_name' => 'required|string|max:255',
        ]);
        $role = new Role();
        $role->roles_name = $request->input('roles_name');
        $role->save();

        return redirect()->route('roles.index')->with('success', 'Role created successfully');
    }

    public function roleEdit(){
        return view('backend.roles.edit');
    }

    public function roleUpdate(){

    }

    public function roleDestroy(){

    }
}
